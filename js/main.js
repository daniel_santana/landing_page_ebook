
(function ($) {
    "use strict";

    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }

        return check;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function downloadURI() {
        let link = document.createElement("a");
        link.download = "ebook.pdf";
        link.href = "./download/ebook.pdf";
        link.click();
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }

    $('#whatsapp').mask('(99) 9 9999-9999');

    $("#subscribe_form").submit(function(event){
        event.preventDefault();
        $('#msgSubscribe').removeClass("alert-success alert-danger show"), 3000;
    
        let post_url = "https://gentle-ocean-14071.herokuapp.com/api/subscribe";
        let request_method = $(this).attr("method");
        let form_data = new FormData(this);
        $.ajax({
            url : post_url,
            type: request_method,
            data : form_data,
            contentType: false,
            cache: false,
            processData:false
        }).done(function(){
            $('#msgSubscribe').addClass("alert-success show"), 3000;
            $('#msgSubscribe').text("Agradecemos por se cadastrar e querer testar nossa plataforma.")
            downloadURI()
    
        }).fail(function() {
            $('#msgSubscribe').addClass("alert-danger show"), 3000;
            $('#msgSubscribe').text("Desculpe, não foi possivel baixar o ebook, tente mais tarde!")
        })
    });

})(jQuery);